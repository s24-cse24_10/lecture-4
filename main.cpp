#include <GL/freeglut_std.h>
#include <iostream>
#include <stdlib.h>
#include <GL/gl.h>
#include <GL/glut.h>
#include <cmath>

using namespace std;

// detecting if point (mx, my) is inside circle with radius centered at (x, y)
// radius >= sqrt((pow((mx - x), 2) + pow((my - y), 2)))

struct Circle {
    // member variables
    float radius;
    float x;
    float y;
    float r;
    float g;
    float b;
    bool selected;

    // default constructor
    Circle() {
        radius = 0.1f;
        x = 0.0f;
        y = 0.0f;
        r = 0.0f;
        g = 0.0f;
        b = 0.0f;
        selected = false;
    }

    // parametrized constructor
    Circle(float _x, float _y, float _radius, float _r, float _g, float _b) {
        x = _x;
        y = _y;
        radius = _radius;
        r = _r;
        g = _g;
        b = _b;
        selected = false;
    }

    void draw() {
        float inc = ( 2 * M_PI) / 60;

        if (selected) {
            glColor3f(1.0f, 0.0f, 1.0f);
        } else {
            glColor3f(r, g, b);
        }
        
        glBegin(GL_POLYGON);
            for (float theta = 0; theta < 2*M_PI; theta += inc){
                glVertex2f(radius * cos(theta) + x, radius * sin(theta) + y);
            }
        glEnd();
    }

    bool isClicked(float mx, float my) {
        if (radius >= sqrt((pow((mx - x), 2) + pow((my - y), 2)))) {
            return true;
        } else {
            return false;
        }
    }
};

Circle circles[5];
int count = 0;

// global variables
int width = 400;
int height = 400;
float r = 0.0f;
float g = 0.0f;
float b = 0.0f;

// convert window coordinates to Cartesian coordinates
void windowToScene(float& x, float& y) {
    x = (2.0f * (x / float(width))) - 1.0f;
    y = 1.0f - (2.0f * (y / float(height)));
}

void drawScene(){
    // Clear the screen and set it to current color (black)
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    for (int i = 0; i < count; i++) {
        circles[i].draw();
    }

    // We have been drawing to the back buffer, put it in the front
    glutSwapBuffers();
}

void mouse(int button, int state, int x, int y) {
    // button: 0 -> left mouse button, 2 -> right mouse button
    // state: 0 -> mouse click, 1 -> mouse release

    // convert window relative coordinates to cartesian coordinates
    float mx = x;
    float my = y;
    windowToScene(mx, my);

    if (button == 0 && state == 0) {

        for (int i = 0; i < count; i++) {
            circles[i].selected = false;
        }

        bool circleClicked = false;
        for (int i = 0; i < count; i++) {
            if (circles[i].isClicked(mx, my)) {
                circles[i].selected = true;
                circleClicked = true;
                break;
            }
        }

        if (count < 5 && !circleClicked) {
            circles[count] = Circle(mx, my, 0.2, r, g, b);
            count++;
        }
        if (count >= 5) {
            cout << "No more circles allowed" << endl;
        }
        glutPostRedisplay();
    }

}

void keyboard(unsigned char key, int x, int y) {
    // esc -> exit program
    if (key == 27) {
        exit(0);
    }

    if (key == 'r') {
        r = 1.0f;
        g = 0.0f;
        b = 0.0f;
    }

    if (key == 'g') {
        r = 0.0f;
        g = 1.0f;
        b = 0.0f;
    }

    if (key == 'b') {
        r = 0.0f;
        g = 0.0f;
        b = 1.0f;
    }

    if (key == 'k') {
        r = 0.0f;
        g = 0.0f;
        b = 0.0f;
    }
}

int main(int argc,char** argv) {
    // Perform some initialization
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB|GLUT_DEPTH);
    glutInitWindowSize(width, height);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("GLUT App");

    // Set the Display Function
    glutDisplayFunc(drawScene);

    // Set the Mouse Function
    glutMouseFunc(mouse);

    // Set the Keyboard Funcion
    glutKeyboardFunc(keyboard);

    // Run the program
    glutMainLoop();

    return 0;
}